Projeto desenvolvido com Javascript, utilizando o framework React juntamente com NextJS,e utilizando a api do Star Wars. O intuito é efetuar o cálculo de quantas paradas será necessária para cada nave percorrer determinada distância. O projeto foi elaborada para entrevista técnica para a vaga de Dev FullStack na Nata.House.

![alt text](https://gitlab.com/lcsdevs/starwars/-/raw/master/assets/mobile.png)

![alt text](https://gitlab.com/lcsdevs/starwars/-/raw/master/assets/desktop.png)

## Tempo gasto

Levei em média dois dias para elaborar o cálculo referente as paradas, no caso converti em horas pois pelas pesquisas entendi que os cálculos envolvendo velocidade da luz, seria correto basear em horas.

## Bugs

Não consegui a tempo incluir o redux para efetuar o cálculo novamente, assim que uma nova request fosse efetuada na paginação da api.

## Tecnologias utilizadas

- Yarn
- React
- NextJS
- Moment, apenas para conversão de horas com mais precisão
- Styled-Components para criação de estilos

## Como rodar

Utilizando o comando abaixo, para instação de todos os pacotes utilizados:

```
yarn
```

Utilizando o comando abaixo, para rodar em modo dev:

```
yarn run dev
```
