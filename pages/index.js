import Header from '../components/header/Header';
import Body from '../components/body/Body';

export default function Index() {
  return (
    <>
      <Header />
      <Body />
    </>
  );
}
